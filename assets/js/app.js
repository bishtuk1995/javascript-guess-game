let computerGuess=0;
let userGuess=[]
let maxiumumGuess ;
let attempt ;
let attemptCount = 0;
let userName;
let userInput = document.querySelector("#input");
let audio = new Audio('/assets/audio/over.wav'); 
let gameOverAudio = new Audio('/assets/audio/game-over.wav'); 
let gameWinSound = new Audio('/assets/audio/win.wav');


const showAttemptCount = (number) => {
    document.querySelector("#attempt").innerText = number;
}


userInput.addEventListener("change", async (event) => {
     

   let userNumberValue=  parseInt(event.target.value);
   attempt=attempt-1;
   attemptCount++;
   userGuess= [...userGuess, userNumberValue];
   document.querySelector("#guess").innerHTML = userGuess.join(", ");
   userInput.value = "";
   showAttemptCount(attemptCount);
 



   if (userNumberValue>computerGuess) {
      document.querySelector("#numberGuessNumberStatus").innerText = `${userName}Your guess is too high`;
   }
   else if (userNumberValue<computerGuess) {
        document.querySelector("#numberGuessNumberStatus").innerText = `${userName}Your guess is too low`;
   }
   else {
        document.querySelector("#numberGuessNumberStatus").innerText = `${userName}  You guessed the correct number`;
        userInput.readOnly=true;
        gameWinSound.play();
        setTimeout(() => {
           window.location.reload();
        }, 5000);
        // document.querySelector("#game-area").style.display = "none";
        // document.querySelector("#congratulation").style.display = "block";
   }
   if (attemptCount=== maxiumumGuess) {
    document.querySelector("#numberGuessNumberStatus").innerText = `oops ${userName} You lose!!. The number was ${computerGuess}, don't mind try again`;
    document.querySelector("#newGameInit").style.display = "block";
   }
   if (attempt==0) {
    userInput.readOnly=true
  await  gameOverAudio.play();
   }
  
});
 


const newGameInit = async() => {
  await audio.play();    
    
     window.location.reload();
}



const init = ( ) => {
    
    computerGuess = Math.floor(Math.random() * 100);
    document.querySelector("#game-area").style.display = "none";
     
}


const startGame = async( ) => {
 await  audio.play();    
    document.querySelector("#welcome-screen").style.display = "none";
    document.querySelector("#game-area").style.display = "block";
    showAttemptCount(attemptCount);

}

const easyMode =async ( ) => {
 await   audio.play();    

 userName =  prompt("What is your name?");
   
    maxiumumGuess = 10;
    attempt=10; 
     startGame();
}


const hardMode = async ( ) => {
   await audio.play();    
 userName =  prompt("What is your name?");

    maxiumumGuess=5; 
    attempt=5;
    startGame();
} 


